/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
*/
package com.getrealhealth.cordova.plugin.android.inappbrowserex;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.Browser;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.DownloadListener;
import android.webkit.HttpAuthHandler;
import android.webkit.URLUtil;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.Config;
import org.apache.cordova.CordovaArgs;
import org.apache.cordova.CordovaHttpAuthHandler;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.LOG;
import org.apache.cordova.PluginManager;
import org.apache.cordova.PluginResult;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.CookieHandler;
import java.net.CookiePolicy;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.StringTokenizer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.widget.Toast;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.content.Context.DOWNLOAD_SERVICE;

@SuppressLint("SetJavaScriptEnabled")
public class InAppBrowserEx extends CordovaPlugin {

  private static final String NULL = "null";
  protected static final String LOG_TAG = "InAppBrowserEx";
  private static final String SELF = "_self";
  private static final String SYSTEM = "_system";
  private static final String EXIT_EVENT = "exit";
  private static final String LOCATION = "location";
  private static final String ZOOM = "zoom";
  private static final String HIDDEN = "hidden";
  private static final String LOAD_START_EVENT = "loadstart";
  private static final String LOAD_STOP_EVENT = "loadstop";
  private static final String LOAD_ERROR_EVENT = "loaderror";
  private static final String CLEAR_ALL_CACHE = "clearcache";
  private static final String CLEAR_SESSION_CACHE = "clearsessioncache";
  private static final String HARDWARE_BACK_BUTTON = "hardwareback";
  private static final String MEDIA_PLAYBACK_REQUIRES_USER_ACTION = "mediaPlaybackRequiresUserAction";
  private static final String SHOULD_PAUSE = "shouldPauseOnSuspend";
  private static final Boolean DEFAULT_HARDWARE_BACK = true;
  private static final String USER_WIDE_VIEW_PORT = "useWideViewPort";
  private static final String TOOLBAR_COLOR = "toolbarcolor";
  private static final String CLOSE_BUTTON_CAPTION = "closebuttoncaption";
  private static final String CLOSE_BUTTON_COLOR = "closebuttoncolor";
  private static final String HIDE_NAVIGATION = "hidenavigationbuttons";
  private static final String NAVIGATION_COLOR = "navigationbuttoncolor";
  private static final String HIDE_URL = "hideurlbar";
  private static final String FOOTER = "footer";
  private static final String FOOTER_COLOR = "footercolor";
  private static final String TITLE = "title";
  private static final String TITLE_COLOR = "titlecolor";
  private static final String CLOSE_BUTTON_IMAGE_PATH = "closeButtonImageRelPath";
  private static final String TOOLBAR_HEIGHT = "toolbarHeight";
  private static final String PHR_URL = "phrUrl";
  private static final String SHELL_URL = "shellUrl";
  private static final String ALLOW_DEVICE_URL = "allowDeviceUrl";
  private static final String BROWSER_BACK_ENABLED = "browserBackEnabled";

  private static final List customizableOptions = Arrays.asList(CLOSE_BUTTON_CAPTION, TOOLBAR_COLOR, NAVIGATION_COLOR, CLOSE_BUTTON_COLOR, FOOTER_COLOR, TITLE, TITLE_COLOR, CLOSE_BUTTON_IMAGE_PATH, TOOLBAR_HEIGHT);

  private static final List<String> deviceHosts = Arrays.asList("fitbit.com",
    "withings.com",
    "garmin.com",
    "ihealthlabs.com",
    "numerasocial.com",
    "ihealthlabs.eu",
    "omronwellness.com");

  private InAppBrowserDialog dialog;
  private WebView inAppWebView;
  private EditText edittext;
  private CallbackContext callbackContext;
  private boolean showLocationBar = true;
  private boolean showZoomControls = true;
  private boolean openWindowHidden = false;
  private boolean clearAllCache = false;
  private boolean clearSessionCache = false;
  private boolean hadwareBackButton = true;
  private boolean browserBack = false;
  private boolean mediaPlaybackRequiresUserGesture = false;
  private boolean shouldPauseInAppBrowser = false;
  private boolean useWideViewPort = true;
  private ValueCallback<Uri> mUploadCallback;
  private ValueCallback<Uri[]> mUploadCallbackLollipop;
  private final static int FILECHOOSER_REQUESTCODE = 1;
  private final static int FILECHOOSER_REQUESTCODE_LOLLIPOP = 2;
  private String closeButtonCaption = "";
  private String closeButtonColor = "";
  private int toolbarColor = android.graphics.Color.LTGRAY;
  private boolean hideNavigationButtons = false;
  private String navigationButtonColor = "";
  private boolean hideUrlBar = false;
  private boolean showFooter = false;
  private String footerColor = "";
  private String[] allowedSchemes;
  private String lastUrl = null;
  private String phrUrl = null;
  private String shellUrl = null;

  private InAppBrowserClient inAppClient;

  @Override
  public void initialize(CordovaInterface cordova, CordovaWebView webView) {
    super.initialize(cordova, webView);
    // your init code here
    java.net.CookieManager cookieManager = new java.net.CookieManager();
    cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
    CookieHandler.setDefault(cookieManager);
    android.webkit.CookieManager webCookieManager =
      CookieManager.getInstance();
    webCookieManager.setAcceptCookie(true);
  }

  /**
   * Executes the request and returns PluginResult.
   *
   * @param action the action to execute.
   * @param args JSONArry of arguments for the plugin.
   * @param callbackContext the callbackContext used when calling back into JavaScript.
   * @return A PluginResult object with a status and message.
   */
  public boolean execute(String action, CordovaArgs args, final CallbackContext callbackContext) throws JSONException {
    if (action.equals("open")) {
      this.callbackContext = callbackContext;
      final String url = args.getString(0);
      String t = args.optString(1);
      if (t == null || t.equals("") || t.equals(NULL)) {
        t = SELF;
      }
      final String target = t;
      final HashMap<String, String> features = parseFeature(args.optString(2));

      LOG.d(LOG_TAG, "target = " + target);

      this.cordova.getActivity().runOnUiThread(new Runnable() {
        @Override
        public void run() {
          String result = "";
          // SELF
          if (SELF.equals(target)) {
            LOG.d(LOG_TAG, "in self");
            /* This code exists for compatibility between 3.x and 4.x versions of Cordova.
             * Previously the Config class had a static method, isUrlWhitelisted(). That
             * responsibility has been moved to the plugins, with an aggregating method in
             * PluginManager.
             */
            Boolean shouldAllowNavigation = null;
            if (url.startsWith("javascript:")) {
              shouldAllowNavigation = true;
            }
            if (shouldAllowNavigation == null) {
              try {
                Method iuw = Config.class.getMethod("isUrlWhiteListed", String.class);
                shouldAllowNavigation = (Boolean) iuw.invoke(null, url);
              } catch (NoSuchMethodException e) {
                LOG.d(LOG_TAG, e.getLocalizedMessage());
              } catch (IllegalAccessException e) {
                LOG.d(LOG_TAG, e.getLocalizedMessage());
              } catch (InvocationTargetException e) {
                LOG.d(LOG_TAG, e.getLocalizedMessage());
              }
            }
            if (shouldAllowNavigation == null) {
              try {
                Method gpm = webView.getClass().getMethod("getPluginManager");
                PluginManager pm = (PluginManager) gpm.invoke(webView);
                Method san = pm.getClass().getMethod("shouldAllowNavigation", String.class);
                shouldAllowNavigation = (Boolean) san.invoke(pm, url);
              } catch (NoSuchMethodException e) {
                LOG.d(LOG_TAG, e.getLocalizedMessage());
              } catch (IllegalAccessException e) {
                LOG.d(LOG_TAG, e.getLocalizedMessage());
              } catch (InvocationTargetException e) {
                LOG.d(LOG_TAG, e.getLocalizedMessage());
              }
            }
            // load in webview
            if (Boolean.TRUE.equals(shouldAllowNavigation)) {
              LOG.d(LOG_TAG, "loading in webview");
              webView.loadUrl(url);
            }
            //Load the dialer
            else if (url.startsWith(WebView.SCHEME_TEL)) {
              try {
                LOG.d(LOG_TAG, "loading in dialer");
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(url));
                cordova.getActivity().startActivity(intent);
              } catch (android.content.ActivityNotFoundException e) {
                LOG.e(LOG_TAG, "Error dialing " + url + ": " + e.toString());
              }
            }
            // load in InAppBrowser
            else {
              LOG.d(LOG_TAG, "loading in InAppBrowser");
              result = showWebPage(url, features);
            }
          }
          // SYSTEM
          else if (SYSTEM.equals(target)) {
            LOG.d(LOG_TAG, "in system");
            result = openExternal(url);
          }
          // BLANK - or anything else
          else {
            LOG.d(LOG_TAG, "in blank");
            result = showWebPage(url, features);
          }

          PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, result);
          pluginResult.setKeepCallback(true);
          callbackContext.sendPluginResult(pluginResult);
        }
      });
    } else if (action.equals("openWithLogin")) {
      this.callbackContext = callbackContext;
      final String autoLoginUrl = args.getString(0);
      String autoLoginHeadersString = args.optString(1);
      String redirectUrl = args.optString(2);
      final String target = SELF;
      final HashMap<String, String> features = parseFeature(args.optString(3));
      final  HashMap<String, String> autoLoginHeaders = parseFeature(autoLoginHeadersString);

      LOG.d(LOG_TAG, "target = " + target);

      int statusCode = this.autoLogin(autoLoginUrl, autoLoginHeaders);


      this.cordova.getActivity().runOnUiThread(new Runnable() {
        @Override
        public void run() {
          String result = "";
          // SELF
          LOG.d(LOG_TAG, "in self");
          /* This code exists for compatibility between 3.x and 4.x versions of Cordova.
           * Previously the Config class had a static method, isUrlWhitelisted(). That
           * responsibility has been moved to the plugins, with an aggregating method in
           * PluginManager.
           */
          Boolean shouldAllowNavigation = null;
          if (redirectUrl.startsWith("javascript:")) {
            shouldAllowNavigation = true;
          }
          if (shouldAllowNavigation == null) {
            try {
              Method iuw = Config.class.getMethod("isUrlWhiteListed", String.class);
              shouldAllowNavigation = (Boolean) iuw.invoke(null, redirectUrl);
            } catch (NoSuchMethodException e) {
              LOG.d(LOG_TAG, e.getLocalizedMessage());
            } catch (IllegalAccessException e) {
              LOG.d(LOG_TAG, e.getLocalizedMessage());
            } catch (InvocationTargetException e) {
              LOG.d(LOG_TAG, e.getLocalizedMessage());
            }
          }
          if (shouldAllowNavigation == null) {
            try {
              Method gpm = webView.getClass().getMethod("getPluginManager");
              PluginManager pm = (PluginManager) gpm.invoke(webView);
              Method san = pm.getClass().getMethod("shouldAllowNavigation", String.class);
              shouldAllowNavigation = (Boolean) san.invoke(pm, redirectUrl);
            } catch (NoSuchMethodException e) {
              LOG.d(LOG_TAG, e.getLocalizedMessage());
            } catch (IllegalAccessException e) {
              LOG.d(LOG_TAG, e.getLocalizedMessage());
            } catch (InvocationTargetException e) {
              LOG.d(LOG_TAG, e.getLocalizedMessage());
            }
          }
          // load in webview
          if (Boolean.TRUE.equals(shouldAllowNavigation)) {
            LOG.d(LOG_TAG, "loading in webview");
            webView.loadUrl(redirectUrl);
          }
          //Load the dialer
          else if (redirectUrl.startsWith(WebView.SCHEME_TEL)) {
            try {
              LOG.d(LOG_TAG, "loading in dialer");
              Intent intent = new Intent(Intent.ACTION_DIAL);
              intent.setData(Uri.parse(redirectUrl));
              cordova.getActivity().startActivity(intent);
            } catch (android.content.ActivityNotFoundException e) {
              LOG.e(LOG_TAG, "Error dialing " + redirectUrl + ": " + e.toString());
            }
          }
          // load in InAppBrowser
          else {
            LOG.d(LOG_TAG, "loading in InAppBrowser");
            result = showWebPage(redirectUrl, features);
          }


          PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, result);
          pluginResult.setKeepCallback(true);
          callbackContext.sendPluginResult(pluginResult);
        }
      });
    } else if (action.equals("close")) {
      closeDialog();
    } else if (action.equals("injectScriptCode")) {
      String jsWrapper = null;
      if (args.getBoolean(1)) {
        jsWrapper = String.format("(function(){prompt(JSON.stringify([eval(%%s)]), 'gap-iab://%s')})()", callbackContext.getCallbackId());
      }
      injectDeferredObject(args.getString(0), jsWrapper);
    } else if (action.equals("injectScriptFile")) {
      String jsWrapper;
      if (args.getBoolean(1)) {
        jsWrapper = String.format("(function(d) { var c = d.createElement('script'); c.src = %%s; c.onload = function() { prompt('', 'gap-iab://%s'); }; d.body.appendChild(c); })(document)", callbackContext.getCallbackId());
      } else {
        jsWrapper = "(function(d) { var c = d.createElement('script'); c.src = %s; d.body.appendChild(c); })(document)";
      }
      injectDeferredObject(args.getString(0), jsWrapper);
    } else if (action.equals("injectStyleCode")) {
      String jsWrapper;
      if (args.getBoolean(1)) {
        jsWrapper = String.format("(function(d) { var c = d.createElement('style'); c.innerHTML = %%s; d.body.appendChild(c); prompt('', 'gap-iab://%s');})(document)", callbackContext.getCallbackId());
      } else {
        jsWrapper = "(function(d) { var c = d.createElement('style'); c.innerHTML = %s; d.body.appendChild(c); })(document)";
      }
      injectDeferredObject(args.getString(0), jsWrapper);
    } else if (action.equals("injectStyleFile")) {
      String jsWrapper;
      if (args.getBoolean(1)) {
        jsWrapper = String.format("(function(d) { var c = d.createElement('link'); c.rel='stylesheet'; c.type='text/css'; c.href = %%s; d.head.appendChild(c); prompt('', 'gap-iab://%s');})(document)", callbackContext.getCallbackId());
      } else {
        jsWrapper = "(function(d) { var c = d.createElement('link'); c.rel='stylesheet'; c.type='text/css'; c.href = %s; d.head.appendChild(c); })(document)";
      }
      injectDeferredObject(args.getString(0), jsWrapper);
    } else if (action.equals("show")) {
      this.cordova.getActivity().runOnUiThread(new Runnable() {
        @Override
        public void run() {
          dialog.show();
        }
      });
      PluginResult pluginResult = new PluginResult(PluginResult.Status.OK);
      pluginResult.setKeepCallback(true);
      this.callbackContext.sendPluginResult(pluginResult);
    } else if (action.equals("load")) {
      String newUrl =  args.getString(0);
      this.cordova.getActivity().runOnUiThread(new Runnable() {
        @Override
        public void run() {
          if(!TextUtils.isEmpty(newUrl) ){
            navigate(newUrl);
          }
          dialog.show();
        }
      });
      PluginResult pluginResult = new PluginResult(PluginResult.Status.OK);
      pluginResult.setKeepCallback(true);
      this.callbackContext.sendPluginResult(pluginResult);
    } else if (action.equals("hide")) {
      this.cordova.getActivity().runOnUiThread(new Runnable() {
        @Override
        public void run() {
          dialog.hide();
        }
      });
      PluginResult pluginResult = new PluginResult(PluginResult.Status.OK);
      pluginResult.setKeepCallback(true);
      this.callbackContext.sendPluginResult(pluginResult);
    } else if (action.equals("stopLoading")) {
      this.cordova.getActivity().runOnUiThread(new Runnable() {
        @Override
        public void run() {
          //no implementation available for inAppWebView.stopLoading(), but still calling it.
          inAppWebView.stopLoading();
        }
      });
      PluginResult pluginResult = new PluginResult(PluginResult.Status.OK);
      pluginResult.setKeepCallback(true);
      this.callbackContext.sendPluginResult(pluginResult);
    } else {
      return false;
    }
    return true;
  }

  private int autoLogin(String strUrl, HashMap<String, String> autoLoginHeaders) {
    int responseCode = 0;
    try {
      URL url = new URL(strUrl);
      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
      if(autoLoginHeaders != null && !autoLoginHeaders.isEmpty()) {
        for (Map.Entry<String, String> item : autoLoginHeaders.entrySet()) {
          connection.setRequestProperty(item.getKey(), item.getValue());
        }
      }

      connection.setRequestMethod("GET");
      responseCode = connection.getResponseCode();

      java.net.CookieStore rawCookieStore = ((java.net.CookieManager)
        CookieHandler.getDefault()).getCookieStore();
      List<HttpCookie> cookies = rawCookieStore.get(url.toURI());
      android.webkit.CookieManager webCookieManager =
        CookieManager.getInstance();
      for (HttpCookie cookie : cookies) {
        String cookieDomain = cookie.getDomain();
        String cookiePath = cookie.getPath();
        String setCookie = new StringBuilder(cookie.toString())
          .append((cookieDomain != null && !cookieDomain.isEmpty() && !url.getHost().toLowerCase().endsWith(cookieDomain.toLowerCase()))? "; domain="+cookieDomain:"")
          .append((cookiePath != null && !cookiePath.isEmpty())? "; path="+cookiePath:"")
          .append("; HttpOnly")
          .toString();
        webCookieManager.setCookie(strUrl, setCookie);
      }

    } catch (ProtocolException e) {
      e.printStackTrace();
    } catch (MalformedURLException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (URISyntaxException e) {
      e.printStackTrace();
    }
    return responseCode;
  }

  /**
   * Called when the view navigates.
   */
  @Override
  public void onReset() {
    closeDialog();
  }

  /**
   * Called when the system is about to start resuming a previous activity.
   */
  @Override
  public void onPause(boolean multitasking) {
    if (shouldPauseInAppBrowser) {
      inAppWebView.onPause();
    }
  }

  /**
   * Called when the activity will start interacting with the user.
   */
  @Override
  public void onResume(boolean multitasking) {
    if (shouldPauseInAppBrowser) {
      inAppWebView.onResume();
    }
  }

  /**
   * Called by AccelBroker when listener is to be shut down.
   * Stop listener.
   */
  public void onDestroy() {
    closeDialog();
  }

  /**
   * Inject an object (script or style) into the InAppBrowser WebView.
   * <p>
   * This is a helper method for the inject{Script|Style}{Code|File} API calls, which
   * provides a consistent method for injecting JavaScript code into the document.
   * <p>
   * If a wrapper string is supplied, then the source string will be JSON-encoded (adding
   * quotes) and wrapped using string formatting. (The wrapper string should have a single
   * '%s' marker)
   *
   * @param source    The source object (filename or script/style text) to inject into
   *                  the document.
   * @param jsWrapper A JavaScript string to wrap the source string in, so that the object
   *                  is properly injected, or null if the source string is JavaScript text
   *                  which should be executed directly.
   */
  private void injectDeferredObject(String source, String jsWrapper) {
    if (inAppWebView != null) {
      String scriptToInject;
      if (jsWrapper != null) {
        org.json.JSONArray jsonEsc = new org.json.JSONArray();
        jsonEsc.put(source);
        String jsonRepr = jsonEsc.toString();
        String jsonSourceString = jsonRepr.substring(1, jsonRepr.length() - 1);
        scriptToInject = String.format(jsWrapper, jsonSourceString);
      } else {
        scriptToInject = source;
      }
      final String finalScriptToInject = scriptToInject;
      this.cordova.getActivity().runOnUiThread(new Runnable() {
        @SuppressLint("NewApi")
        @Override
        public void run() {
          if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            // This action will have the side-effect of blurring the currently focused element
            inAppWebView.loadUrl("javascript:" + finalScriptToInject);
          } else {
            inAppWebView.evaluateJavascript(finalScriptToInject, null);
          }
        }
      });
    } else {
      LOG.d(LOG_TAG, "Can't inject code into the system browser");
    }
  }

  /**
   * Put the list of features into a hash map
   *
   * @param optString
   * @return
   */
  private HashMap<String, String> parseFeature(String optString) {
    if (optString.equals(NULL)) {
      return null;
    } else {
      HashMap<String, String> map = new HashMap<String, String>();
      StringTokenizer features = new StringTokenizer(optString, ",");
      StringTokenizer option;
      while (features.hasMoreElements()) {
        option = new StringTokenizer(features.nextToken(), "=");
        if (option.hasMoreElements()) {
          String key = option.nextToken();
          String value = option.nextToken();
          if (!customizableOptions.contains(key)) {
            //value = value.equals("yes") || value.equals("no") ? value : "yes";
          }
          map.put(key, value);
        }
      }
      return map;
    }
  }

  /**
   * Display a new browser with the specified URL.
   *
   * @param url the url to load.
   * @return "" if ok, or error message.
   */
  public String openExternal(String url) {
    try {
      Intent intent = null;
      intent = new Intent(Intent.ACTION_VIEW);
      // Omitting the MIME type for file: URLs causes "No Activity found to handle Intent".
      // Adding the MIME type to http: URLs causes them to not be handled by the downloader.
      Uri uri = Uri.parse(url);
      if ("file".equals(uri.getScheme())) {
        intent.setDataAndType(uri, webView.getResourceApi().getMimeType(uri));
      } else {
        intent.setData(uri);
      }
      intent.putExtra(Browser.EXTRA_APPLICATION_ID, cordova.getActivity().getPackageName());
      this.cordova.getActivity().startActivity(intent);
      return "";
      // not catching FileUriExposedException explicitly because buildtools<24 doesn't know about it
    } catch (java.lang.RuntimeException e) {
      LOG.d(LOG_TAG, "InAppBrowser: Error loading url " + url + ":" + e.toString());
      return e.toString();
    }
  }

  /**
   * Closes the dialog
   */
  public void closeDialog() {
    this.cordova.getActivity().runOnUiThread(new Runnable() {
      @Override
      public void run() {
		if(inAppClient != null) {
          inAppClient.hideLoader();
        }
        final WebView childView = inAppWebView;
        // The JS protects against multiple calls, so this should happen only when
        // closeDialog() is called by other native code.
        if (childView == null) {
          return;
        }

        childView.setWebViewClient(new WebViewClient() {
          // NB: wait for about:blank before dismissing
          public void onPageFinished(WebView view, String url) {
            if (dialog != null) {
              dialog.dismiss();
              dialog = null;
            }
          }
        });
        // NB: From SDK 19: "If you call methods on WebView from any thread
        // other than your app's UI thread, it can cause unexpected results."
        // http://developer.android.com/guide/webapps/migrating.html#Threads
        childView.loadUrl("about:blank");

        try {
          JSONObject obj = new JSONObject();
          obj.put("type", EXIT_EVENT);
          sendUpdate(obj, false);
        } catch (JSONException ex) {
          LOG.d(LOG_TAG, "Should never happen");
        }
      }
    });
  }

  /**
   * Checks to see if it is possible to go back one page in history, then does so.
   */
  public void goBack() {
    if (this.inAppWebView.canGoBack()) {
      this.inAppWebView.goBack();
    }
  }

  /**
   * Can the web browser go back?
   *
   * @return boolean
   */
  public boolean canGoBack() {
    return this.inAppWebView.canGoBack();
  }

  /**
   * Has the user set the hardware back button to go back
   *
   * @return boolean
   */
  public boolean hardwareBack() {
    return hadwareBackButton;
  }

  /**
   * Has the user set the browser back to go back
   *
   * @return boolean
   */
  public boolean browserBack() {
    return browserBack;
  }

  /**
   * Checks to see if it is possible to go forward one page in history, then does so.
   */
  private void goForward() {
    if (this.inAppWebView.canGoForward()) {
      this.inAppWebView.goForward();
    }
  }

  /**
   * Navigate to the new page
   *
   * @param url to load
   */
  private void navigate(String url) {
    InputMethodManager imm = (InputMethodManager) this.cordova.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
    imm.hideSoftInputFromWindow(edittext.getWindowToken(), 0);

    if (!url.startsWith("http") && !url.startsWith("file:")) {
      this.inAppWebView.loadUrl("http://" + url);
    } else {
      this.inAppWebView.loadUrl(url);
    }
    this.inAppWebView.requestFocus();
  }


  /**
   * Should we show the location bar?
   *
   * @return boolean
   */
  private boolean getShowLocationBar() {
    return this.showLocationBar;
  }

  private InAppBrowserEx getInAppBrowser() {
    return this;
  }


  /**
   * Display a new browser with the specified URL.
   *
   * @param url      the url to load.
   * @param features jsonObject
   */
  public String showWebPage(final String url, HashMap<String, String> features) {
    // Determine if we should hide the location bar.
    showLocationBar = true;
    showZoomControls = true;
    openWindowHidden = false;
    mediaPlaybackRequiresUserGesture = false;

    if (features != null) {
      String show = features.get(LOCATION);
      if (show != null) {
        showLocationBar = show.equals("yes") ? true : false;
      }
      showLocationBar = true;
      if (showLocationBar) {
        String hideNavigation = features.get(HIDE_NAVIGATION);
        String hideUrl = features.get(HIDE_URL);
        if (hideNavigation != null)
          hideNavigationButtons = hideNavigation.equals("yes") ? true : false;
        if (hideUrl != null) hideUrlBar = hideUrl.equals("yes") ? true : false;
      }
      String zoom = features.get(ZOOM);
      if (zoom != null) {
        showZoomControls = zoom.equals("yes") ? true : false;
      }
      String hidden = features.get(HIDDEN);
      if (hidden != null) {
        openWindowHidden = hidden.equals("yes") ? true : false;
      }
      String hardwareBack = features.get(HARDWARE_BACK_BUTTON);
      if (hardwareBack != null) {
        hadwareBackButton = hardwareBack.equals("yes") ? true : false;
      } else {
        hadwareBackButton = DEFAULT_HARDWARE_BACK;
      }
      String browserBackEnabled = features.get(BROWSER_BACK_ENABLED);
      if (browserBackEnabled != null) {
        browserBack = browserBackEnabled.equals("yes") ? true : false;
      }
      String mediaPlayback = features.get(MEDIA_PLAYBACK_REQUIRES_USER_ACTION);
      if (mediaPlayback != null) {
        mediaPlaybackRequiresUserGesture = mediaPlayback.equals("yes") ? true : false;
      }
      String cache = features.get(CLEAR_ALL_CACHE);
      if (cache != null) {
        clearAllCache = cache.equals("yes") ? true : false;
      } else {
        cache = features.get(CLEAR_SESSION_CACHE);
        if (cache != null) {
          clearSessionCache = cache.equals("yes") ? true : false;
        }
      }
      String shouldPause = features.get(SHOULD_PAUSE);
      if (shouldPause != null) {
        shouldPauseInAppBrowser = shouldPause.equals("yes") ? true : false;
      }
      String wideViewPort = features.get(USER_WIDE_VIEW_PORT);
      if (wideViewPort != null) {
        useWideViewPort = wideViewPort.equals("yes") ? true : false;
      }
      String closeButtonCaptionSet = features.get(CLOSE_BUTTON_CAPTION);
      if (closeButtonCaptionSet != null) {
        closeButtonCaption = closeButtonCaptionSet;
      }
      String closeButtonColorSet = features.get(CLOSE_BUTTON_COLOR);
      if (closeButtonColorSet != null) {
        closeButtonColor = closeButtonColorSet;
      }
      String toolbarColorSet = features.get(TOOLBAR_COLOR);
      if (toolbarColorSet != null) {
        toolbarColor = android.graphics.Color.parseColor(toolbarColorSet);
      }
      String navigationButtonColorSet = features.get(NAVIGATION_COLOR);
      if (navigationButtonColorSet != null) {
        navigationButtonColor = navigationButtonColorSet;
      }
      String showFooterSet = features.get(FOOTER);
      if (showFooterSet != null) {
        showFooter = showFooterSet.equals("yes") ? true : false;
      }
      String footerColorSet = features.get(FOOTER_COLOR);
      if (footerColorSet != null) {
        footerColor = footerColorSet;
      }
    }

    final CordovaWebView thatWebView = this.webView;


    // Create dialog in new thread
    Runnable runnable = new Runnable() {
      /**
       * Convert our DIP units to Pixels
       *
       * @return int
       */
      private int dpToPixels(int dipValue) {
        int value = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
          (float) dipValue,
          cordova.getActivity().getResources().getDisplayMetrics()
        );

        return value;
      }


      private Drawable getImage(String name, double altDensity) throws IOException {
        Drawable result = null;
        Resources activityRes = cordova.getActivity().getResources();

        if (name != null) {
          File file = new File("www", name);
          InputStream is = null;
          try {
            is = cordova.getActivity().getAssets().open(file.getPath());
            Bitmap bitmap = BitmapFactory.decodeStream(is);
            bitmap.setDensity((int) (DisplayMetrics.DENSITY_MEDIUM * altDensity));
            result = new BitmapDrawable(activityRes, bitmap);
          } finally {
            // Make sure we close this input stream to prevent resource leak.
            try {
              is.close();
            } catch (Exception e) {
            }
          }
        }
        return result;
      }

      private View createCloseButton(int id) {
        View _close;
        Resources activityRes = cordova.getActivity().getResources();

        if (closeButtonCaption != "") {
          // Use TextView for text
          TextView close = new TextView(cordova.getActivity());
          close.setText(closeButtonCaption);
          close.setTextSize(20);
          if (closeButtonColor != "")
            close.setTextColor(android.graphics.Color.parseColor(closeButtonColor));
          close.setGravity(android.view.Gravity.CENTER_VERTICAL);
          close.setPadding(this.dpToPixels(10), 0, this.dpToPixels(10), 0);
          _close = close;
        } else {

          Drawable closeImage = null;
          try {
            String closeImageSet = features.get(CLOSE_BUTTON_IMAGE_PATH);
            if (closeImageSet != null) {
              closeImage = getImage(closeImageSet, 1.0);
              if (closeImage != null) {
                closeImage = new BitmapDrawable(cordova.getContext().getResources(), Bitmap.createScaledBitmap(((BitmapDrawable) closeImage).getBitmap(), 32, 32, true));
              }
            }
          } catch (Exception e) {
          }

          ImageButton close = new ImageButton(cordova.getActivity());
          if (closeImage == null) {
            int closeResId = activityRes.getIdentifier("ic_action_remove", "drawable", cordova.getActivity().getPackageName());
            closeImage = activityRes.getDrawable(closeResId);
          }
          if (closeButtonColor != "")
            close.setColorFilter(android.graphics.Color.parseColor(closeButtonColor));
          close.setImageDrawable(closeImage);
          close.setScaleType(ImageView.ScaleType.FIT_CENTER);
          if (Build.VERSION.SDK_INT >= 16)
            close.getAdjustViewBounds();

          _close = close;
        }

        RelativeLayout.LayoutParams closeLayoutParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        closeLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        _close.setLayoutParams(closeLayoutParams);

        if (Build.VERSION.SDK_INT >= 16)
          _close.setBackground(null);
        else
          _close.setBackgroundDrawable(null);

        _close.setContentDescription("Close Button");
        _close.setId(Integer.valueOf(id));
        _close.setOnClickListener(new View.OnClickListener() {
          public void onClick(View v) {
            if (browserBack())
              dialog.onBackPressed();
            else
              closeDialog();
          }
        });

        return _close;
      }

      @SuppressLint("NewApi")
      public void run() {

        // CB-6702 InAppBrowser hangs when opening more than one instance
        if (dialog != null) {
          dialog.dismiss();
        }


        // Let's create the main dialog
        dialog = new InAppBrowserDialog(cordova.getActivity(), android.R.style.Theme_NoTitleBar);
        dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setInAppBroswer(getInAppBrowser());

        // Main container layout
        LinearLayout main = new LinearLayout(cordova.getActivity());
        main.setOrientation(LinearLayout.VERTICAL);

        // Toolbar layout
        RelativeLayout toolbar = new RelativeLayout(cordova.getActivity());
        //Please, no more black!
        toolbar.setBackgroundColor(toolbarColor);
        int toolbarHeight = 44;
        String toolbarHeightSet = features.get(TOOLBAR_HEIGHT);
        if (toolbarHeightSet != null) {
          toolbarHeight = Integer.parseInt(toolbarHeightSet);
        }
        toolbar.setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, this.dpToPixels(toolbarHeight)));
        toolbar.setPadding(this.dpToPixels(2), this.dpToPixels(2), this.dpToPixels(2), this.dpToPixels(2));
        toolbar.setHorizontalGravity(Gravity.LEFT);
        toolbar.setVerticalGravity(Gravity.CENTER);

        View bottomBorder = new View(cordova.getActivity());
        bottomBorder.setBackgroundColor(Color.GRAY);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 1);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        toolbar.addView(bottomBorder, params);

        boolean showTitle = false;
        String titleText = features.get(TITLE);
        showTitle = titleText != null;

        // Action Button Container layout
        RelativeLayout actionButtonContainer = new RelativeLayout(cordova.getActivity());
        RelativeLayout.LayoutParams actionButtonContainerLayoutParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        actionButtonContainerLayoutParams.addRule(RelativeLayout.RIGHT_OF, showTitle ? 7 : 4);
        actionButtonContainer.setLayoutParams(actionButtonContainerLayoutParams);
        actionButtonContainer.setHorizontalGravity(Gravity.LEFT);
        actionButtonContainer.setVerticalGravity(Gravity.CENTER_VERTICAL);
        actionButtonContainer.setId(Integer.valueOf(1));

        // Back button
        ImageButton back = new ImageButton(cordova.getActivity());
        RelativeLayout.LayoutParams backLayoutParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        backLayoutParams.addRule(RelativeLayout.ALIGN_LEFT);
        back.setLayoutParams(backLayoutParams);
        back.setContentDescription("Back Button");
        back.setId(Integer.valueOf(2));
        Resources activityRes = cordova.getActivity().getResources();
        int backResId = activityRes.getIdentifier("ic_action_previous_item", "drawable", cordova.getActivity().getPackageName());
        Drawable backIcon = activityRes.getDrawable(backResId);
        if (navigationButtonColor != "")
          back.setColorFilter(android.graphics.Color.parseColor(navigationButtonColor));
        if (Build.VERSION.SDK_INT >= 16)
          back.setBackground(null);
        else
          back.setBackgroundDrawable(null);
        back.setImageDrawable(backIcon);
        back.setScaleType(ImageView.ScaleType.FIT_CENTER);
        back.setPadding(0, this.dpToPixels(10), 0, this.dpToPixels(10));
        if (Build.VERSION.SDK_INT >= 16)
          back.getAdjustViewBounds();

        back.setOnClickListener(new View.OnClickListener() {
          public void onClick(View v) {
            goBack();
          }
        });

        // Forward button
        ImageButton forward = new ImageButton(cordova.getActivity());
        RelativeLayout.LayoutParams forwardLayoutParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        forwardLayoutParams.addRule(RelativeLayout.RIGHT_OF, 2);
        forward.setLayoutParams(forwardLayoutParams);
        forward.setContentDescription("Forward Button");
        forward.setId(Integer.valueOf(3));
        int fwdResId = activityRes.getIdentifier("ic_action_next_item", "drawable", cordova.getActivity().getPackageName());
        Drawable fwdIcon = activityRes.getDrawable(fwdResId);
        if (navigationButtonColor != "")
          forward.setColorFilter(android.graphics.Color.parseColor(navigationButtonColor));
        if (Build.VERSION.SDK_INT >= 16)
          forward.setBackground(null);
        else
          forward.setBackgroundDrawable(null);
        forward.setImageDrawable(fwdIcon);
        forward.setScaleType(ImageView.ScaleType.FIT_CENTER);
        forward.setPadding(0, this.dpToPixels(10), 0, this.dpToPixels(10));
        if (Build.VERSION.SDK_INT >= 16)
          forward.getAdjustViewBounds();

        forward.setOnClickListener(new View.OnClickListener() {
          public void onClick(View v) {
            goForward();
          }
        });


        // Edit Text Box
        edittext = new EditText(cordova.getActivity());
        RelativeLayout.LayoutParams textLayoutParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        textLayoutParams.addRule(RelativeLayout.RIGHT_OF, 5);
textLayoutParams.addRule((RelativeLayout.CENTER_IN_PARENT));

        textLayoutParams.addRule(RelativeLayout.LEFT_OF, 1);
        edittext.setLayoutParams(textLayoutParams);
        edittext.setId(Integer.valueOf(4));
        edittext.setGravity(Gravity.CENTER_VERTICAL | Gravity.BOTTOM);
        edittext.setSingleLine(true);
        edittext.setText(features.get(TITLE));
        edittext.setEllipsize(TextUtils.TruncateAt.END);

        edittext.setTextColor(Color.BLACK);
        edittext.setInputType(InputType.TYPE_TEXT_VARIATION_NORMAL);
        edittext.setImeOptions(EditorInfo.IME_ACTION_GO);
        edittext.setInputType(InputType.TYPE_NULL); // Will not except input... Makes the text NON-EDITABLE
        edittext.setBackgroundResource(android.R.color.transparent);
        edittext.setOnKeyListener(new View.OnKeyListener() {
          public boolean onKey(View v, int keyCode, KeyEvent event) {
            // If the event is a key-down event on the "enter" button
            if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
              navigate(edittext.getText().toString());
              return true;
            }
            return false;
          }
        });

        if (showTitle) {
          toolbar.addView(edittext);
        }

        // Header Close/Done button
        View close = createCloseButton(5);
        toolbar.addView(close);

        // Footer
        RelativeLayout footer = new RelativeLayout(cordova.getActivity());
        int _footerColor;
        if (footerColor != "") {
          _footerColor = Color.parseColor(footerColor);
        } else {
          _footerColor = android.graphics.Color.LTGRAY;
        }
        footer.setBackgroundColor(_footerColor);
        RelativeLayout.LayoutParams footerLayout = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, this.dpToPixels(44));
        footerLayout.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        footer.setLayoutParams(footerLayout);
        if (closeButtonCaption != "")
          footer.setPadding(this.dpToPixels(8), this.dpToPixels(8), this.dpToPixels(8), this.dpToPixels(8));
        footer.setHorizontalGravity(Gravity.LEFT);
        footer.setVerticalGravity(Gravity.BOTTOM);

        View footerClose = createCloseButton(7);
        footer.addView(footerClose);


        // WebView
        inAppWebView = new WebView(cordova.getActivity());
        inAppWebView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        inAppWebView.setId(Integer.valueOf(6));
        // File Chooser Implemented ChromeClient
        inAppWebView.setWebChromeClient(new InAppChromeClient(thatWebView) {
          // For Android 5.0+
          public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams) {
            LOG.d(LOG_TAG, "File Chooser 5.0+");
            // If callback exists, finish it.
            if (mUploadCallbackLollipop != null) {
              mUploadCallbackLollipop.onReceiveValue(null);
            }
            mUploadCallbackLollipop = filePathCallback;

            // Create File Chooser Intent
            Intent content = new Intent(Intent.ACTION_GET_CONTENT);
            content.addCategory(Intent.CATEGORY_OPENABLE);
            content.setType("*/*");

            // Run cordova startActivityForResult
            cordova.startActivityForResult(InAppBrowserEx.this, Intent.createChooser(content, "Select File"), FILECHOOSER_REQUESTCODE_LOLLIPOP);
            return true;
          }

          // For Android 4.1+
          public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
            LOG.d(LOG_TAG, "File Chooser 4.1+");
            // Call file chooser for Android 3.0+
            openFileChooser(uploadMsg, acceptType);
          }

          // For Android 3.0+
          public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
            LOG.d(LOG_TAG, "File Chooser 3.0+");
            mUploadCallback = uploadMsg;
            Intent content = new Intent(Intent.ACTION_GET_CONTENT);
            content.addCategory(Intent.CATEGORY_OPENABLE);

            // run startActivityForResult
            cordova.startActivityForResult(InAppBrowserEx.this, Intent.createChooser(content, "Select File"), FILECHOOSER_REQUESTCODE);
          }

        });
        WebViewClient client = new InAppBrowserClient(thatWebView, edittext, features);
        inAppClient = (InAppBrowserClient) client;
        inAppWebView.setWebViewClient(client);
        WebSettings settings = inAppWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setBuiltInZoomControls(showZoomControls);
        settings.setPluginState(android.webkit.WebSettings.PluginState.ON);

        inAppWebView.setDownloadListener(new DownloadListener() {

          @Override
          public void onDownloadStart(String url, String userAgent,
                                      String contentDisposition, String mimeType,
                                      long contentLength) {

            if (cordova.hasPermission(WRITE_EXTERNAL_STORAGE)) {
              //final WebViewClient client = inAppWebView.getWebViewClient();
              inAppClient.showLoader();

              DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));

              request.setMimeType(mimeType);
              //------------------------COOKIE!!------------------------
              String cookies = CookieManager.getInstance().getCookie(url);
              request.addRequestHeader("cookie", cookies);
              //------------------------COOKIE!!------------------------
              request.addRequestHeader("User-Agent", userAgent);
              //request.setDescription("Downloading file...");
              request.setTitle(URLUtil.guessFileName(url, contentDisposition, mimeType));
              request.allowScanningByMediaScanner();
              request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
              request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, URLUtil.guessFileName(url, contentDisposition, mimeType));
              final DownloadManager dm = (DownloadManager) cordova.getActivity().getSystemService(DOWNLOAD_SERVICE);
              final long downloadId = dm.enqueue(request);
              Toast.makeText(cordova.getActivity().getApplicationContext(), "Downloading...", Toast.LENGTH_LONG).show();

              final int UPDATE_PROGRESS = 5020;


              @SuppressLint("HandlerLeak") final Handler handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                  super.handleMessage(msg);
                }
              };
              new Thread(new Runnable() {
                @Override
                public void run() {
                  boolean downloading = true;
                  while (downloading) {
                    DownloadManager.Query q = new DownloadManager.Query();
                    q.setFilterById(downloadId);
                    Cursor cursor = dm.query(q);
                    cursor.moveToFirst();
                    int bytes_downloaded = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                    int bytes_total = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
                    if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                      downloading = false;
                    }

                    Message msg = handler.obtainMessage();
                    msg.what = UPDATE_PROGRESS;

                    msg.arg1 = bytes_downloaded;
                    msg.arg2 = bytes_total;
                    handler.sendMessage(msg);
                    cursor.close();
                  }
                  inAppClient.hideLoader();
                }
              }).start();

            } else {
              cordova.requestPermission(InAppBrowserEx.this, 200, WRITE_EXTERNAL_STORAGE);
            }

          }
        });

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
          settings.setMediaPlaybackRequiresUserGesture(mediaPlaybackRequiresUserGesture);
        }

        String overrideUserAgent = preferences.getString("OverrideUserAgent", null);
        String appendUserAgent = preferences.getString("AppendUserAgent", null);

        if (overrideUserAgent != null) {
          settings.setUserAgentString(overrideUserAgent);
        }
        if (appendUserAgent != null) {
          settings.setUserAgentString(settings.getUserAgentString() + appendUserAgent);
        }

        //Toggle whether this is enabled or not!
        Bundle appSettings = cordova.getActivity().getIntent().getExtras();
        boolean enableDatabase = appSettings == null ? true : appSettings.getBoolean("InAppBrowserStorageEnabled", true);
        if (enableDatabase) {
          String databasePath = cordova.getActivity().getApplicationContext().getDir("inAppBrowserDB", Context.MODE_PRIVATE).getPath();
          settings.setDatabasePath(databasePath);
          settings.setDatabaseEnabled(true);
        }
        settings.setDomStorageEnabled(true);

        if (clearAllCache) {
          CookieManager.getInstance().removeAllCookie();
        } else if (clearSessionCache) {
          CookieManager.getInstance().removeSessionCookie();
        }

        // Enable Thirdparty Cookies on >=Android 5.0 device
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
          CookieManager.getInstance().setAcceptThirdPartyCookies(inAppWebView, true);
        }

        inAppWebView.loadUrl(url);
        inAppWebView.setId(Integer.valueOf(6));
        inAppWebView.getSettings().setLoadWithOverviewMode(true);
        inAppWebView.getSettings().setUseWideViewPort(useWideViewPort);
        inAppWebView.requestFocus();
        inAppWebView.requestFocusFromTouch();

        // Add the back and forward buttons to our action button container layout
        actionButtonContainer.addView(back);
        actionButtonContainer.addView(forward);

        // Add the views to our toolbar if they haven't been disabled
        //if (!hideNavigationButtons) toolbar.addView(actionButtonContainer);
        //if (!hideUrlBar && !showTitle) toolbar.addView(edittext);

        // Don't add the toolbar if its been disabled
        if (getShowLocationBar()) {
          // Add our toolbar to our main view/layout
          main.addView(toolbar);
        }

        // Add our webview to our main view/layout
        RelativeLayout webViewLayout = new RelativeLayout(cordova.getActivity());
        webViewLayout.addView(inAppWebView);
        main.addView(webViewLayout);

        // Don't add the footer unless it's been enabled
        if (showFooter) {
          webViewLayout.addView(footer);
        }

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;

        dialog.setContentView(main);
        dialog.show();
        dialog.getWindow().setAttributes(lp);
        // the goal of openhidden is to load the url and not display it
        // Show() needs to be called to cause the URL to be loaded
        if (openWindowHidden) {
          dialog.hide();
        }
      }
    };
    this.cordova.getActivity().runOnUiThread(runnable);
    return "";
  }

  @Override
  public void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults) throws JSONException {

    if (permissions != null && permissions.length > 0) {
      boolean hasAllPermissions = hasAllPermissions(permissions);
      if (hasAllPermissions && lastUrl != null) {
        inAppWebView.stopLoading();
        inAppWebView.loadUrl(lastUrl);
      } else {
      }
    }
  }

  private boolean hasAllPermissions(String[] permissions) throws JSONException {

    for (String permission : permissions) {
      if (!cordova.hasPermission(permission)) {
        return false;
      }
    }

    return true;
  }

  /**
   * Create a new plugin success result and send it back to JavaScript
   *
   * @param obj a JSONObject contain event payload information
   */
  private void sendUpdate(JSONObject obj, boolean keepCallback) {
    sendUpdate(obj, keepCallback, PluginResult.Status.OK);
  }

  /**
   * Create a new plugin result and send it back to JavaScript
   *
   * @param obj    a JSONObject contain event payload information
   * @param status the status code to return to the JavaScript environment
   */
  private void sendUpdate(JSONObject obj, boolean keepCallback, PluginResult.Status status) {
    if (callbackContext != null) {
      PluginResult result = new PluginResult(status, obj);
      result.setKeepCallback(keepCallback);
      callbackContext.sendPluginResult(result);
      if (!keepCallback) {
        callbackContext = null;
      }
    }
  }

  /**
   * Receive File Data from File Chooser
   *
   * @param requestCode the requested code from chromeclient
   * @param resultCode  the result code returned from android system
   * @param intent      the data from android file chooser
   */
  public void onActivityResult(int requestCode, int resultCode, Intent intent) {
    // For Android >= 5.0
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      LOG.d(LOG_TAG, "onActivityResult (For Android >= 5.0)");
      // If RequestCode or Callback is Invalid
      if (requestCode != FILECHOOSER_REQUESTCODE_LOLLIPOP || mUploadCallbackLollipop == null) {
        super.onActivityResult(requestCode, resultCode, intent);
        return;
      }
      mUploadCallbackLollipop.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, intent));
      mUploadCallbackLollipop = null;
    }
    // For Android < 5.0
    else {
      LOG.d(LOG_TAG, "onActivityResult (For Android < 5.0)");
      // If RequestCode or Callback is Invalid
      if (requestCode != FILECHOOSER_REQUESTCODE || mUploadCallback == null) {
        super.onActivityResult(requestCode, resultCode, intent);
        return;
      }

      if (null == mUploadCallback) return;
      Uri result = intent == null || resultCode != cordova.getActivity().RESULT_OK ? null : intent.getData();

      mUploadCallback.onReceiveValue(result);
      mUploadCallback = null;
    }
  }

  /**
   * The webview client receives notifications about appView
   */
  public class InAppBrowserClient extends WebViewClient {
    EditText edittext;
    CordovaWebView webView;
    ProgressDialog progressDialog;
    HashMap<String, String> features;

    /**
     * Constructor.
     *
     * @param webView
     * @param mEditText
     */
    public InAppBrowserClient(CordovaWebView webView, EditText mEditText, HashMap<String, String> features) {
      this.webView = webView;
      this.edittext = mEditText;
      this.features = features;
    }

    public void showLoader(){

      if(progressDialog != null && progressDialog.isShowing()){
        return;
      }

      Runnable runnable = new Runnable() {
        public void run() {
          progressDialog = new ProgressDialog(cordova.getContext());
          try {
            progressDialog.show();


          } catch (WindowManager.BadTokenException e) {
          }


          progressDialog.setCancelable(false);

          Objects.requireNonNull(progressDialog.getWindow())
            .setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

          cordova.getActivity().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
          RelativeLayout relativeLayout = new RelativeLayout(cordova.getContext());

          RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.MATCH_PARENT,
            RelativeLayout.LayoutParams.MATCH_PARENT);

          ProgressBar progressBar = new ProgressBar(cordova.getContext());

          RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.WRAP_CONTENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT);
          lp.addRule(RelativeLayout.CENTER_IN_PARENT);

          progressBar.setLayoutParams(lp);

          Objects.requireNonNull(progressDialog.getWindow())
            .clearFlags(LayoutParams.FLAG_DIM_BEHIND);

          relativeLayout.addView(progressBar);

          progressDialog.setContentView(relativeLayout, rlp);
          //return dialog;

        }
      };
      cordova.getActivity().runOnUiThread(runnable);
    }

    public void  hideLoader(){
      if(progressDialog != null){
        progressDialog.dismiss();
        progressDialog = null;
      }
    }

    /**
     * Override the URL that should be loaded
     * <p>
     * This handles a small subset of all the URIs that would be encountered.
     *
     * @param webView
     * @param url
     */
    @Override
    public boolean shouldOverrideUrlLoading(WebView webView, String url) {
      lastUrl = url;
      String allowDevice = features.get(ALLOW_DEVICE_URL);
      if(allowDevice != "yes" && isDeviceUrl(url)){
        try {
          JSONObject obj = new JSONObject();
          obj.put("type", LOAD_START_EVENT);
          obj.put("url", url);
          sendUpdate(obj, true);
        } catch (JSONException ex) {
          LOG.e(LOG_TAG, "URI passed in has caused a JSON error.");
        }
        return true;
      }
      if (url.startsWith(WebView.SCHEME_TEL)) {
        try {
          Intent intent = new Intent(Intent.ACTION_DIAL);
          intent.setData(Uri.parse(url));
          cordova.getActivity().startActivity(intent);
          return true;
        } catch (android.content.ActivityNotFoundException e) {
          LOG.e(LOG_TAG, "Error dialing " + url + ": " + e.toString());
        }
      } else if (url.startsWith("geo:") || url.startsWith(WebView.SCHEME_MAILTO) || url.startsWith("market:")) {
        try {
          Intent intent = new Intent(Intent.ACTION_VIEW);
          intent.setData(Uri.parse(url));
          cordova.getActivity().startActivity(intent);
          return true;
        } catch (android.content.ActivityNotFoundException e) {
          LOG.e(LOG_TAG, "Error with " + url + ": " + e.toString());
        }
      }
      else if(url.startsWith("intent:")){
        try {
          Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
          String fallbackUrl = intent.getStringExtra("browser_fallback_url");
          if (fallbackUrl != null) {
            openExternal(fallbackUrl);
            return true;
          } else {
            try {
              intent = new Intent(Intent.ACTION_VIEW);
              intent.setData(Uri.parse(url));
              cordova.getActivity().startActivity(intent);
              return true;
            } catch (android.content.ActivityNotFoundException e) {
              LOG.e(LOG_TAG, "Error with " + url + ": " + e.toString());
            }
          }
        }
        catch (URISyntaxException e) {
          LOG.e(LOG_TAG, "Error with " + url + ": " + e.toString());
        }

      }
      // If sms:5551212?body=This is the message
      else if (url.startsWith("sms:")) {
        try {
          Intent intent = new Intent(Intent.ACTION_VIEW);

          // Get address
          String address = null;
          int parmIndex = url.indexOf('?');
          if (parmIndex == -1) {
            address = url.substring(4);
          } else {
            address = url.substring(4, parmIndex);

            // If body, then set sms body
            Uri uri = Uri.parse(url);
            String query = uri.getQuery();
            if (query != null) {
              if (query.startsWith("body=")) {
                intent.putExtra("sms_body", query.substring(5));
              }
            }
          }
          intent.setData(Uri.parse("sms:" + address));
          intent.putExtra("address", address);
          intent.setType("vnd.android-dir/mms-sms");
          cordova.getActivity().startActivity(intent);
          return true;
        } catch (android.content.ActivityNotFoundException e) {
          LOG.e(LOG_TAG, "Error sending sms " + url + ":" + e.toString());
        }
      }
      // Test for whitelisted custom scheme names like mycoolapp:// or twitteroauthresponse:// (Twitter Oauth Response)
      else if (!url.startsWith("http:") && !url.startsWith("https:") && url.matches("^[a-z]*://.*?$")) {
        if (allowedSchemes == null) {
          String allowed = preferences.getString("AllowedSchemes", "");
          allowedSchemes = allowed.split(",");
        }

        if (allowedSchemes != null) {
          for (String scheme : allowedSchemes) {
            if (url.startsWith(scheme)) {
              try {
                JSONObject obj = new JSONObject();
                obj.put("type", "customscheme");
                obj.put("url", url);
                sendUpdate(obj, true);
                return true;
              } catch (JSONException ex) {
                LOG.e(LOG_TAG, "Custom Scheme URI passed in has caused a JSON error.");
              }
            }
          }
        }
      }
      showLoader();
      return false;
    }

    boolean isDeviceUrl(String url){
      Uri uri = Uri.parse(url);
      String hostName = uri.getHost().toLowerCase();
      for (String host: deviceHosts
           ) {
        if(hostName.contains(host)){
          return true;
        }
      }
      return false;
    }


    /*
     * onPageStarted fires the LOAD_START_EVENT
     *
     * @param view
     * @param url
     * @param favicon
     */
    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
      super.onPageStarted(view, url, favicon);
      String newloc = "";
      if (url.startsWith("http:") || url.startsWith("https:") || url.startsWith("file:")) {
        newloc = url;
      } else {
        // Assume that everything is HTTP at this point, because if we don't specify,
        // it really should be.  Complain loudly about this!!!
        LOG.e(LOG_TAG, "Possible Uncaught/Unknown URI");
        newloc = "http://" + url;
      }

      // Update the UI if we haven't already
      String phrUrl = features.get(PHR_URL);
      String shellUrl = features.get(SHELL_URL);
      Uri currentUri = Uri.parse(url);

      boolean location = features.get(LOCATION) == null ? false : features.get(LOCATION).equals("yes");
      if(location && phrUrl != null && shellUrl != null && !phrUrl.contains(currentUri.getHost()) && !shellUrl.contains(currentUri.getHost())){
          if (!newloc.equals(edittext.getText().toString())) {
            edittext.setText(newloc);
          }
      } else {
        edittext.setText(features.get(TITLE));
      }


      try {
        JSONObject obj = new JSONObject();
        obj.put("type", LOAD_START_EVENT);
        obj.put("url", newloc);
        sendUpdate(obj, true);
      } catch (JSONException ex) {
        LOG.e(LOG_TAG, "URI passed in has caused a JSON error.");
      }

      showLoader();
    }



    public void onPageFinished(WebView view, String url) {
      super.onPageFinished(view, url);

      hideLoader();

      // CB-10395 InAppBrowser's WebView not storing cookies reliable to local device storage
      if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
        CookieManager.getInstance().flush();
      } else {
        CookieSyncManager.getInstance().sync();
      }

      // https://issues.apache.org/jira/browse/CB-11248
      view.clearFocus();
      view.requestFocus();

      try {
        JSONObject obj = new JSONObject();
        obj.put("type", LOAD_STOP_EVENT);
        obj.put("url", url);

        sendUpdate(obj, true);
      } catch (JSONException ex) {
        LOG.d(LOG_TAG, "Should never happen");
      }
    }

    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
      super.onReceivedError(view, errorCode, description, failingUrl);
      hideLoader();
      try {
        JSONObject obj = new JSONObject();
        obj.put("type", LOAD_ERROR_EVENT);
        obj.put("url", failingUrl);
        obj.put("code", errorCode);
        obj.put("message", description);

        sendUpdate(obj, true, PluginResult.Status.ERROR);
      } catch (JSONException ex) {
        LOG.d(LOG_TAG, "Should never happen");
      }
    }

    /**
     * On received http auth request.
     */
    @Override
    public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {

      // Check if there is some plugin which can resolve this auth challenge
      PluginManager pluginManager = null;
      try {
        Method gpm = webView.getClass().getMethod("getPluginManager");
        pluginManager = (PluginManager) gpm.invoke(webView);
      } catch (NoSuchMethodException e) {
        LOG.d(LOG_TAG, e.getLocalizedMessage());
      } catch (IllegalAccessException e) {
        LOG.d(LOG_TAG, e.getLocalizedMessage());
      } catch (InvocationTargetException e) {
        LOG.d(LOG_TAG, e.getLocalizedMessage());
      }

      if (pluginManager == null) {
        try {
          Field pmf = webView.getClass().getField("pluginManager");
          pluginManager = (PluginManager) pmf.get(webView);
        } catch (NoSuchFieldException e) {
          LOG.d(LOG_TAG, e.getLocalizedMessage());
        } catch (IllegalAccessException e) {
          LOG.d(LOG_TAG, e.getLocalizedMessage());
        }
      }

      if (pluginManager != null && pluginManager.onReceivedHttpAuthRequest(webView, new CordovaHttpAuthHandler(handler), host, realm)) {
        return;
      }

      // By default handle 401 like we'd normally do!
      super.onReceivedHttpAuthRequest(view, handler, host, realm);
    }

  }
}
