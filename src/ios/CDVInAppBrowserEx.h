/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 */

#import <Cordova/CDVPlugin.h>
#import <Cordova/CDVInvokedUrlCommand.h>
#import <Cordova/CDVScreenOrientationDelegate.h>
#import <WebKit/WebKit.h>

@class CDVInAppBrowserExViewController;

@interface CDVInAppBrowserEx : CDVPlugin {
    UIWindow * tmpWindow;

    @private
    NSString* _beforeload;
    BOOL _waitForBeforeload;
    BOOL allowdeviceurl;
    BOOL browserbackenabled;
}

@property (nonatomic, retain) CDVInAppBrowserEx* instance;


+ (id) getInstance;

- (void)loadAfterBeforeload:(CDVInvokedUrlCommand*)command;

@property (nonatomic, retain) CDVInAppBrowserExViewController* inAppBrowserViewController;
@property (nonatomic, copy) NSString* callbackId;
@property (nonatomic, copy) NSRegularExpression *callbackIdPattern;
@property (nonatomic, retain) NSURLSession *urlSession;

- (void)open:(CDVInvokedUrlCommand*)command;
- (void)close:(CDVInvokedUrlCommand*)command;
- (void)injectScriptCode:(CDVInvokedUrlCommand*)command;
- (void)show:(CDVInvokedUrlCommand*)command;
- (void)hide:(CDVInvokedUrlCommand*)command;
- (void)exportFile:(CDVInvokedUrlCommand*)command;
- (void)autoLogin:(CDVInvokedUrlCommand*)command;
- (void)load:(CDVInvokedUrlCommand*)command;
- (void)stopLoading:(CDVInvokedUrlCommand*)command;
@end

@interface CDVInAppBrowserOptionsEx : NSObject {}

@property (nonatomic, assign) BOOL location;
@property (nonatomic, assign) BOOL toolbar;
@property (nonatomic, copy) NSString* closebuttoncaption;
@property (nonatomic, copy) NSString* closebuttoncolor;
@property (nonatomic, assign) BOOL lefttoright;
@property (nonatomic, copy) NSString* toolbarposition;
@property (nonatomic, copy) NSString* toolbarcolor;
@property (nonatomic, assign) BOOL toolbartranslucent;
@property (nonatomic, assign) BOOL hidenavigationbuttons;
@property (nonatomic, copy) NSString* navigationbuttoncolor;
@property (nonatomic, assign) BOOL cleardata;
@property (nonatomic, assign) BOOL clearcache;
@property (nonatomic, assign) BOOL clearsessioncache;
@property (nonatomic, assign) BOOL hidespinner;

@property (nonatomic, copy) NSString* presentationstyle;
@property (nonatomic, copy) NSString* transitionstyle;

@property (nonatomic, assign) BOOL enableviewportscale;
@property (nonatomic, assign) BOOL mediaplaybackrequiresuseraction;
@property (nonatomic, assign) BOOL allowinlinemediaplayback;
@property (nonatomic, assign) BOOL hidden;
@property (nonatomic, assign) BOOL disallowoverscroll;
@property (nonatomic, copy) NSString* beforeload;

@property (nonatomic, copy) NSString* title;
@property (nonatomic, copy) NSString* titlecolor;
@property (nonatomic, copy) NSString* closebuttonimagerelpath;
@property (nonatomic, assign) double toolbarheight;
@property (nonatomic, copy) NSString* stoponurlcontains;
@property (nonatomic, copy) NSString* phrurl;
@property (nonatomic, copy) NSString* shellurl;
@property (nonatomic, assign) BOOL allowdeviceurl;
@property (nonatomic, assign) BOOL browserbackenabled;

+ (CDVInAppBrowserOptionsEx*)parseOptions:(NSString*)options;

@end

@interface CDVWKInAppBrowserExUIDelegate : NSObject <WKUIDelegate>{
    @private
    UIViewController* _viewController;
}

@property (nonatomic, copy) NSString* title;

- (instancetype)initWithTitle:(NSString*)title;
-(void) setViewController:(UIViewController*) viewController;

@end

@interface CDVInAppBrowserExViewController : UIViewController<CDVScreenOrientationDelegate,WKNavigationDelegate,WKUIDelegate,WKScriptMessageHandler>{
    @private
    CDVInAppBrowserOptionsEx *_browserOptions;
    UIView* _statusBarBackgroundView;
    NSDictionary *_settings;
}



@property (nonatomic, strong) IBOutlet UIBarButtonItem* closeButton;
@property (nonatomic, strong) IBOutlet UILabel* addressLabel;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* backButton;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* forwardButton;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView* spinner;
@property (nonatomic, strong) IBOutlet UIToolbar* toolbar;
@property (nonatomic, strong) IBOutlet UILabel* popupTitle;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* addressToolbarItem;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* titleToolbarItem;


@property (nonatomic, weak) id <CDVScreenOrientationDelegate> orientationDelegate;
@property (nonatomic, weak) CDVInAppBrowserEx* navigationDelegate;
@property (nonatomic) NSURL* currentURL;



@property (nonatomic, strong) IBOutlet WKWebView* webView;
@property (nonatomic, strong) IBOutlet WKWebViewConfiguration* configuration;


@property (nonatomic, strong) IBOutlet CDVWKInAppBrowserExUIDelegate* webViewUIDelegate;




- (void)close;
- (void)navigateTo:(NSURL*)url;
- (void)showLocationBar:(BOOL)show;
- (void)showToolBar:(BOOL)show : (NSString *) toolbarPosition;
- (void)setCloseButtonTitle:(NSString*)title : (NSString*) colorString : (int) buttonIndex;

- (id)initWithBrowserOptions: (CDVInAppBrowserOptionsEx*) browserOptions andSettings:(NSDictionary*) settings;

@end

@interface CDVInAppBrowserNavigationControllerEx : UINavigationController

@property (nonatomic, weak) id <CDVScreenOrientationDelegate> orientationDelegate;

@end


